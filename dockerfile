FROM ubuntu
USER root
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN wget https://builds.openlogic.com/downloadJDK/openlogic-openjdk-jre/8u342-b07/openlogic-openjdk-jre-8u342-b07-linux-x64-deb.deb
RUN dpkg -i openlogic-openjdk-jre-8u342-b07-linux-x64-deb.deb
RUN apt install r-base pocl-opencl-icd -y
RUN apt install pandoc texlive-full -y
RUN Rscript -e "install.packages('rmarkdown')"
RUN Rscript -e "install.packages('properties')"
ADD app /opt/app
ADD Headless_full_pack /

CMD ["bash","createBook.sh"]

