Growth-Grammar-Related Interactive Modelling Platform
-----------------------------------------------------

GroIMP is an extensible interactive modelling platform, written in Java.
It is OSI Certified Open Source Software. Source and binary distributions
are available at the web page

    http://www.grogra.de/

For instructions on how to build and install GroIMP, see the INSTALL file
of the distribution.

If you have further questions, you can consult the web page
http://www.grogra.de/ or send an e-mail to info at grogra.de. To get the
latest news about GroIMP, subscribe to our newsletter which can be found
at the web page.
