#!/bin/bash
if [ $1 == '-r' ]
then
	Rscript -e "rmarkdown::render('report.rmd',output_file = sprintf('report_%s.pdf',Sys.time()),output_dir='output/')"
elif [ $1 == '-h' ]
then
	java -Xss1g -Xms1g -Xmx1g   -jar /home/tim/programme/GroIMP/dev/app/core.jar --headless  model/green_houes_tomato.gsz
else
	java -Xss1g -Xms1g -Xmx1g   -jar /home/tim/programme/GroIMP/dev/app/core.jar  model/green_houes_tomato.gsz
fi
