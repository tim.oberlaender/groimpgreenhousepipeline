---
title: "GreenHouse Report"
output: pdf_document
date: "`r Sys.time()`"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
system("bash runner_headless.sh")
# read in sensor log
library(properties)
config<-read.properties("config.properties")

data.sensor<-read.csv("output/sensor_log.csv")
data.size<-read.csv("output/plant_log_size.csv")
data.energy<-read.csv("output/plant_log_energy.csv")
data.area <- read.csv("output/plant_log_area.csv")
data.relativ <- read.csv("output/plant_log_relativ.csv")

```

# Digital Greenhouse

Summary of the collected data from the greenhouse Simulation, based on 

- `config.properties` 
- `r config$GrowPlan` 
- `r config$SensorList`




```{r,echo=FALSE, results='asis'}

if(as.logical(trimws(config$sensors))){
  hour <- as.numeric(trimws(config$hour))
  doy <- as.numeric(trimws(config$doy))
  hourt<-hour
  cat("# Sensor\n\n")
  if(as.logical(trimws(config$sensorHeatMap))){
    map.wid<-as.numeric(trimws(config$sensorHeatMapWidth))
    map.hig<-as.numeric(trimws(config$sensorHeatMapLength))
    
    cat("## heatmap\n\n")
    for (i in c(1:length(data.sensor[,1]))){
      if(sum(data.sensor[i,])>0){
        cat("### sensors at hour: ",hourt," of day: ",doy," :\n\n")
        heatmap(array(unlist(data.sensor[i,]),dim=c(map.hig,map.wid)),Rowv = NA, Colv = NA, )
        cat("\n\n")
      }
      hourt<-hourt+1
      if(hourt>24){
          hourt <-0
          doy<-doy+1
        
      }
    }
  }


  if(as.logical(trimws(config$sensorSinglePlot))){    
    start<-hour
    label_h<-c()
    for(i in c(0:length(data.sensor[,1]))){
      label_h[i]<-start 
      start<-start+1
      if(start>24){
        start<-0
      }
      
    }
    cat("\n\n")
    cat("## Individual Sensors \n\n")
    for (i in c(1:length(data.sensor[1,]))){
      cat("### ",names(data.sensor)[i],"\nn")
      plot(data.sensor[,i],type = "l", xaxt = "n")
      axis(1, at=1:length(data.sensor[,1]), labels=label_h)
      cat("\n\n")
    }
  }
  if(as.logical(trimws(config$sensorComparePlot))){ 
    cat("## Compare Sensors \n\n")
    plot(data.sensor[,1],type = "l", ylim =c(min(data.sensor),max(data.sensor)),col=1, lwd=2, xaxt = "n")
    axis(1, at=1:length(data.sensor[,1]), labels=label_h)
    for (i in c(2:length(data.sensor[1,]))){
      lines(data.sensor[,i],col=i,lwd=2)
    }
    legend("topleft", legend=names(data.sensor),
    col=c(1:length(data.sensor[1,])), lty = 1:2, cex=0.8)
    cat("\n\n\n")
    barplot(colMeans(data.sensor))
    
  }
}
```

```{r echo=FALSE,  results='asis'}
if(as.logical(trimws(config$plants))){
cat ("# Single Plant reports\n\n")
for (i in c(1:length(data.size[1,]))){
  cat("## Plant ",names(data.size)[i],"\n\n")
#  par(mfrow = c(2, 2))
  plot(data.size[,i],main="Size of plant",type = "l")
  plot(data.energy[,i],main="POV",type = "l")
  plot(data.area[,i],main="Leaf area of plant",type = "l")
  plot(data.relativ[,i],main="POV per square",type = "l")

  cat("\n\n")

  }

cat("# Compare Plants\n\n")

cat("## POV\n\n")

plot(data.energy[,1],type = "l", ylim =c(min(data.energy),max(data.energy)),col=1, lwd=2)
for (i in c(2:length(data.energy[1,]))){
  lines(data.energy[,i],col=i,lwd=2)
  }
legend("topleft", legend=names(data.size),
       col=c(1:length(data.size[1,])), lty = 1:2, cex=0.8)



cat("\n\n")
cat("## POV by square meter\n\n")

plot(data.relativ[,1],type = "l",ylim=c(min(data.relativ),max(data.relativ)),col=1, lwd=2)
for (i in c(2:length(data.relativ[1,]))){
  lines(data.relativ[,i],col=i,lwd=2)
  }
legend("topleft", legend=names(data.size),
       col=c(1:length(data.size[1,])), lty = 1:2, cex=0.8)



cat("\n\n")
cat("## Height\n\n")

plot(data.size[,1],type = "l",ylim=c(min(data.size),max(data.size)),col=1, lwd=2)
for (i in c(2:length(data.size[1,]))){
  lines(data.size[,i],col=i,lwd=2)
  }
legend("topleft", legend=names(data.size),
       col=c(1:length(data.size[1,])), lty = 1:2, cex=0.8)

cat("\n\n")
cat("## Leaf Area\n\n")
plot(data.area[,1],type = "l", ylim=c(min(data.area),max(data.area)),col=1, lwd=2)
for (i in c(2:length(data.area[1,]))){
  lines(data.area[,i],col=i,lwd=2)
  }
legend("topleft", legend=names(data.size),
       col=c(1:length(data.size[1,])), lty = 1:2, cex=0.8)

}
```